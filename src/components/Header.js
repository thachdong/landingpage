import React from 'react';
import {Container} from "reactstrap";
import LanguageIcon from "../assets/images/language-vietnamese.png";
import Logo from "../assets/images/logo.png";

export default function Header() {
  return (
    <Container className="header-block px-0 " fluid={true}>
      <div className="header-block__logo">
        <img src={Logo} alt="logo" />
      </div>
      <div className="header-block__navbar">
        <ul >
          <li>
            <a href="/home" alt="home">Home</a>
          </li>
          <li>
            <a href="/about" alt="About">About</a>
          </li>
          <li>
            <a href="/contact" alt="Contact">Contact</a>   
          </li>
          <li className="language-box">
            <img src= {LanguageIcon} alt="language icon" />
            <select value="Vietnamese">
              <option value="Vietnamese">Vietnamese</option>
            </select>
          </li>
        </ul>
      </div>
      <div className="header-block__content">
        <h3>Cách mạng hóa giáo dục</h3>
        <p>Nâng tầm trải nghiệm học tập với nền tảng quản lý lớp học mạnh mẽ, tích hợp các công nghệ tiên tiến phục vụ tối đa cho việc giảng dạy và học tập</p>
        <div className="cta-box">
          <a href="/tutor" alt="Bắt đầu giảng dạy ngay hôm nay">Bắt đầu giảng dạy ngay hôm nay</a>
          <a href="/student" alt="Tìm gia sư phù hợp ngay hôm nay">Tìm gia sư phù hợp ngay hôm nay</a>
        </div>
      </div>
    </Container>
  )
}