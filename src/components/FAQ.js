import React, { useState } from "react";
import { Container, Collapse } from "reactstrap";
import Icon from "../assets/images/faqIcon.png";
import Image from "../assets/images/faq.png";

export default function Faq() {
  const [collapse, setCollapse] = useState({
    criteria1: false,
    criteria2: false,
    criteria3: false,
  });
  const toggle = (key, value) => {
    const updateCollapse = { ...collapse };
    updateCollapse[key] = value;
    setCollapse({ ...updateCollapse });
  };
  return (
    <Container className="faq-block px-0" fluid={true}>
      <div className="faq-block__icon">
        <img src={Icon} alt="faq" />
      </div>
      <h3 className="faq-block__header">Những câu hỏi thường gặp</h3>
      <div className="faq-block__content">
        <div className="content-container">
          <div className="content-box">
            <div
              className="button"
              onClick={() => toggle("criteria1", !collapse.criteria1)}
            >
              <span className="text">Tôi có thể tìm gia sư cho những môn học nào?</span>
              <span
                className={
                  collapse.criteria1
                    ? "caret-icon caret-up"
                    : "caret-icon caret-down"
                }
              ></span>
            </div>
            <Collapse isOpen={collapse.criteria1}>
            Tại AKADON, kiến thức là vô tận để bạn tự do khám phá, học hỏi và phát triển bản thân. Bạn có thể tìm gia sư cho các môn học phổ thông như Toán, Lý, Hóa, Văn, Anh.... Ngoài ra bạn có thể học hỏi từ các chuyên gia hàng đầu về các kỹ năng mềm và kỹ năng cứng cần thiết trong công việc như giao tiếp, thuyết trình, thương lượng, lập trình, phân tích dữ liệu, excel, powerpoint... Bạn cũng có thể làm phong phú thêm cuộc sống của mình với các môn học nghệ thuật như piano, guitar, vẽ tranh, yoga...
            </Collapse>
          </div>

          <div className="content-box">
            <div
              className="button"
              onClick={() => toggle("criteria2", !collapse.criteria2)}
            >
              <span className="text">Tôi có thể học tập qua những hình thức nào?</span>
              <span
                className={
                  collapse.criteria2
                    ? "caret-icon caret-up"
                    : "caret-icon caret-down"
                }
              ></span>
            </div>
            <Collapse isOpen={collapse.criteria2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor ac mauris, ullamcorper
            </Collapse>
          </div>

          <div className="content-box">
            <div
              className="button"
              onClick={() => toggle("criteria3", !collapse.criteria3)}
            >
              <span className="text">Thu nhập trung bình của gia sư hàng tháng là bao nhiêu?</span>
              <span
                className={
                  collapse.criteria3
                    ? "caret-icon caret-up"
                    : "caret-icon caret-down"
                }
              ></span>
            </div>
            <Collapse isOpen={collapse.criteria3}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor ac mauris, ullamcorper
            </Collapse>
          </div>

        </div>
        <div className="image-container">
          <img src={Image} alt="faq" />
        </div>
      </div>
    </Container>
  );
}
