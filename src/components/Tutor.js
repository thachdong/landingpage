import React, { useState } from "react";
import { Container, Collapse } from "reactstrap";
import Image from "../assets/images/tutor.png";
import Icon from "../assets//images/studentIcon.png";
import BlockIcon from "../assets/images/tutorIcon.png";

export default function Student() {
  const [collapse, setCollapse] = useState({
    criteria1: false,
    criteria2: false,
    criteria3: false,
  });
  const toggle = (key, value) => {
    const updateCollapse = { ...collapse };
    updateCollapse[key] = value;
    setCollapse({ ...updateCollapse });
  };
  return (
    <Container className="service-detail pt-0" style={{background: "#fff"}} fluid={true}>
      <div className="service-detail__icon">
        <img src={BlockIcon} alt="service detail icon" />
      </div>
      <div className="service-detail__image mr-3">
        <img src={Image} width={603} alt="student block" />
      </div>
      <div className="service-detail__content">
        <h3>Với gia sư</h3>
        <div className="content-wraper">
          <div className="content">
            <div
              className="button"
              onClick={() => toggle("criteria1", !collapse.criteria1)}
            >
              <img src={Icon} alt="student" />
              <span className="text">Chủ động</span>
              <span
                className={
                  collapse.criteria1
                    ? "caret-icon caret-up"
                    : "caret-icon caret-down"
                }
              ></span>
            </div>
            <Collapse isOpen={collapse.criteria1}>
              Text discription here
            </Collapse>
          </div>

          <div className="content">
            <div
              className="button"
              onClick={() => toggle("criteria2", !collapse.criteria2)}
            >
              <img src={Icon} alt="student" />
              <span className="text">Tự do</span>
              <span
                className={
                  collapse.criteria2
                    ? "caret-icon caret-up"
                    : "caret-icon caret-down"
                }
              ></span>
            </div>
            <Collapse isOpen={collapse.criteria2}>Text discription here</Collapse>
          </div>

          <div className="content">
            <div
              className="button"
              onClick={() => toggle("criteria3", !collapse.criteria3)}
            >
              <img src={Icon} alt="student" />
              <span className="text">Biến những gì bạn giỏi thành nguồn thu nhập không giới hạn</span>
              <span
                className={
                  collapse.criteria3
                    ? "caret-icon caret-up"
                    : "caret-icon caret-down"
                }
              ></span>
            </div>
            <Collapse isOpen={collapse.criteria3}>Text discription here</Collapse>
          </div>

        </div>
        <p className="note">Bạn tự tin vào kiến thức của mình?</p>
        <a className="cta tutor" href="/tutor" alt="Đăng ký dạy để chia sẻ kiến thức của bạn">Đăng ký dạy để chia sẻ kiến thức của bạn</a>
      </div>
    </Container>
  );
}
