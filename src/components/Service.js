import React from 'react';
import {
  Card, CardImg, CardText,
  CardTitle, Container
} from 'reactstrap';
import Icon0 from "../assets/images/service-0.png";
import Icon1 from "../assets/images/service-1.png";
import Icon2 from "../assets/images/service-2.png";
import Icon3 from "../assets/images/service-3.png";
import Icon4 from "../assets/images/service-4.png";

export default function Service() {
  return (
    <Container className="service-block px-0" fluid={true}>
      <div className="service-block__icon">
        <img src={Icon0} alt="service block icon" />
      </div>
      <h3>Giá trị chung của dịch vụ</h3>
      <div className="service-block__content">
        <Card>
          <CardImg src={Icon1} />
          <CardTitle>Quản lý học tập</CardTitle>
          <CardText>Platform quản lý học tập toàn diện dành cho gia sư và học viên</CardText>
        </Card>
        <Card>
          <CardImg src={Icon2} />
          <CardTitle>Quản lý học tập</CardTitle>
          <CardText>Platform quản lý học tập toàn diện dành cho gia sư và học viên</CardText>
        </Card>
        <Card>
          <CardImg src={Icon3} />
          <CardTitle>Quản lý học tập</CardTitle>
          <CardText>Platform quản lý học tập toàn diện dành cho gia sư và học viên</CardText>
        </Card>
        <Card>
          <CardImg src={Icon4} />
          <CardTitle>Quản lý học tập</CardTitle>
          <CardText>Platform quản lý học tập toàn diện dành cho gia sư và học viên</CardText>
        </Card>
      </div>
    </Container>
  )
}
