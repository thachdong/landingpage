import React from 'react';
import {Container} from "reactstrap";

export default function Invite() {
  return (
    <Container className="invite-block px-0" fluid={true}>
      <h3 className="invite-block__header">Hãy cùng chúng tôi bắt đầu <br /> trải nghiệm học tập tuyệt vời của bạn!</h3>
      <div className="invite-block__cta-box">
        <a className="tutor" href="/tutor" alt="tutor">Bắt đầu giảng dạy ngay hôm nay</a>
        <a className="student" href="/student" alt="student">Tìm gia sư phù hợp ngay hôm nay</a>
      </div>
    </Container>
  )
}
