import React from 'react';
import {Container} from "reactstrap";

export default function Process() {
  return (
    <Container className="process-block px-0" fluid={true}>
      <h3 className="process-block__header">Quy trình</h3>
      <div className="process-block__content">

        <div className="stage">
          <span>1</span>
          <p>Học viên đăng yêu cầu tìm gia sư</p>
        </div>
        <div className="step-sign"></div>

        <div className="stage">
          <span className="odd">2</span>
          <p>Gia sư nhận yêu cầu tương ứng với chuyên môn của mình và đăng ký dạy</p>
        </div>
        <div className="step-sign even"></div>

        <div className="stage">
          <span>3</span>
          <p>Học viên lựa chọn gia sư phù hợp từ danh sách đăng ký và gửi lời mời dạy</p>
        </div>
        <div className="step-sign"></div>

        <div className="stage">
          <span className="odd">4</span>
          <p>Gia sư và học viên lựa chọn phương thức học (online hoặc offline) theo nhu cầu</p>
        </div>
        <div className="step-sign even"></div>

        <div className="stage">
          <span>5</span>
          <p>Học viên thanh toán phí cho gia sư sau mỗi buổi dạy</p>
        </div>
      </div>
      <div className="process-block__data">
        <div className="data-name">
          <span className="volum volum-style-1">678</span>
          <span className="name">Số lượng gia sư</span>
        </div>

        <div className="data-name">
          <span className="volum volum-style-2">5025</span>
          <span className="name">Số lượng gia sư</span>
        </div>

        <div className="data-name">
          <span className="volum volum volum-style-3">7000</span>
          <span className="name">Số lượng gia sư</span>
        </div>

        <div className="data-name">
          <span className="volum volum volum-style-4">320</span>
          <span className="name">Số lượng gia sư</span>
        </div>

        <div className="data-name">
          <span className="volum volum volum-style-5">25</span>
          <span className="name">Số lượng gia sư</span>
        </div>
      </div>
    </Container>
  )
}
