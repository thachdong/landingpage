import React from 'react';
import {Container} from "reactstrap";
import Logo from "../assets/images/logoTransparent.png";

export default function Footer() {
  return (
    <Container className="footer-block px-0" fluid={true}>
      <h3 className="footer-block__header">Bạn có câu hỏi dành cho chúng tôi?</h3>
      <form className="footer-block__form">
        <input type="text" className="input" placeholder="nguyen.hau@akadon.com.vn" />
        <button className="button">Send</button>
      </form>
      <ul className="footer-block__contact mb-0">
        <li>
          <div className="contact">
            <img src={Logo} alt="logo" />
            <a href="tel:0333876897">+84 333 876 897</a>
            <a href="mailto:contact@akadon.com.vn">contact@akadon.com.vn</a>
          </div>
        </li>
        <li>
          <a href="/facebook">FACEBOOK</a>
        </li>
        <li>
          <a href="/facebook">INSTAGRAM</a>
        </li>
        <li>
          <a href="/facebook">TWITTER</a>
        </li>
        <li>
          <a href="/facebook">GOOGLE</a>
        </li>
      </ul>
    </Container>
  )
}
