import React, { useState } from "react";
import { Container, Collapse } from "reactstrap";
import Image from "../assets/images/student.png";
import Icon from "../assets//images/studentIcon.png";

export default function Student() {
  const [collapse, setCollapse] = useState({
    prestige: false,
    cost: false,
    quality: false,
    quickly: false,
    convenient: false,
    option: false,
  });
  const toggle = (key, value) => {
    const updateCollapse = { ...collapse };
    updateCollapse[key] = value;
    setCollapse({ ...updateCollapse });
  };
  return (
    <Container className="service-detail" fluid={true}>
      <div className="service-detail__content">
        <h3>Với học viên</h3>
        <div className="content-wraper">
          <div className="content">
            <div
              className="button"
              onClick={() => toggle("prestige", !collapse.prestige)}
            >
              <img src={Icon} alt="student" />
              <span className="text">Uy tín</span>
              <span
                className={
                  collapse.prestige
                    ? "caret-icon caret-up"
                    : "caret-icon caret-down"
                }
              ></span>
            </div>
            <Collapse isOpen={collapse.prestige}>
              Text discription here
            </Collapse>
          </div>

          <div className="content">
            <div
              className="button"
              onClick={() => toggle("cost", !collapse.cost)}
            >
              <img src={Icon} alt="student" />
              <span className="text">Tùy chọn chi phí</span>
              <span
                className={
                  collapse.cost
                    ? "caret-icon caret-up"
                    : "caret-icon caret-down"
                }
              ></span>
            </div>
            <Collapse isOpen={collapse.cost}>Text discription here</Collapse>
          </div>

          <div className="content">
            <div
              className="button"
              onClick={() => toggle("quality", !collapse.quality)}
            >
              <img src={Icon} alt="student" />
              <span className="text">Chất lượng</span>
              <span
                className={
                  collapse.quality
                    ? "caret-icon caret-up"
                    : "caret-icon caret-down"
                }
              ></span>
            </div>
            <Collapse isOpen={collapse.quality}>Text discription here</Collapse>
          </div>

          <div className="content">
            <div
              className="button"
              onClick={() => toggle("quickly", !collapse.quickly)}
            >
              <img src={Icon} alt="student" />
              <span className="text">Nhanh chóng</span>
              <span
                className={
                  collapse.quickly
                    ? "caret-icon caret-up"
                    : "caret-icon caret-down"
                }
              ></span>
            </div>
            <Collapse isOpen={collapse.quickly}>Text discription here</Collapse>
          </div>

          <div className="content">
            <div
              className="button"
              onClick={() => toggle("convenient", !collapse.convenient)}
            >
              <img src={Icon} alt="student" />
              <span className="text">Tiện lợi</span>
              <span
                className={
                  collapse.convenient
                    ? "caret-icon caret-up"
                    : "caret-icon caret-down"
                }
              ></span>
            </div>
            <Collapse isOpen={collapse.convenient}>
              Text discription here
            </Collapse>
          </div>

          <div className="content">
            <div
              className="button"
              onClick={() => toggle("option", !collapse.option)}
            >
              <img src={Icon} alt="student" />
              <span className="text">Đa dạng lựa chọn</span>
              <span
                className={
                  collapse.option
                    ? "caret-icon caret-up"
                    : "caret-icon caret-down"
                }
              ></span>
            </div>
            <Collapse isOpen={collapse.option}>Text discription here</Collapse>
          </div>
        </div>
        <p className="note">Bạn mong muốn được hỗ trợ trong học tập?</p>
        <a className="cta" href="/tutor" alt="Đăng yêu cầu tìm gia sư">Đăng yêu cầu tìm gia sư</a>
      </div>
      <div className="service-detail__image text-right">
        <img src={Image} alt="student block" />
      </div>
    </Container>
  );
}