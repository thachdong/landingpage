import React from "react";
import { Container } from "reactstrap";
import Header from "./components/Header";
import Service from "./components/Service";
import Student from "./components/Student";
import Tutor from "./components/Tutor";
import Process from "./components/Process";
import Faq from "./components/Faq";
import Invite from "./components/Invite";
import Footer from "./components/Footer";

function App() {
  return (
    <Container className="px-0" fluid={true}>
      <Header />
      <Service />
      <Student />
      <Tutor />
      <Process />
      <Faq />
      <Invite />
      <Footer />
    </Container>
  );
}

export default App;
